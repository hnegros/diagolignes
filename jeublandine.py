import itertools

# plateau = [
#     [True,False,True,False],
#     [False,True,True,True],
#     [True,True,True,False],
#     [False,True,False,True],
# ]

plateau = [
    [False,False,True,False],
    [True,False,True,False],
    [False,True,True,True],
    [True,True,True,False],
    [False,True,False,True],
    [False,True,False,False],
]

# plateau = [
#     [False,False,False,True],
#     [True,False,True,False],
#     [False,True,True,True],
#     [True,True,True,False],
#     [False,True,False,True],
#     [True,False,False,False],
# ]


def nb_cases(pla):
    nb_ca = 0
    for i in pla:
        for j in i:
            if j:
                nb_ca+=1

    return nb_ca


def score_diagonale(plateau):
    score = 0
    for iligne in range(len(plateau)):
        for icase in range(len(plateau[0])):
            if plateau[iligne][icase] is not None:
                try:
                    if plateau[iligne][icase] and plateau[iligne+1][icase+1] and plateau[iligne+1][icase+1] is not None :
                        score += 1 
                        # print("d {} {}".format(iligne,icase))
                except IndexError:
                    pass
                try:
                    if plateau[iligne][icase] and icase -1 >= 0 and plateau[iligne+1][icase-1] and plateau[iligne+1][icase-1] is not None:
                        score += 1
                        # print("g {} {}".format(iligne,icase))
                except IndexError:
                    pass
                try:
                    if not plateau[iligne][icase] and not plateau[iligne+1][icase+1] and plateau[iligne+1][icase+1] is not None :
                        score += 1 
                        # print("d {} {}".format(iligne,icase))
                except IndexError:
                    pass
                try:
                    if not plateau[iligne][icase] and icase -1 >= 0 and not plateau[iligne+1][icase-1] and plateau[iligne+1][icase-1] is not None:
                        score += 1
                        # print("g {} {}".format(iligne,icase))
                except IndexError:
                    pass
    return score

def score_ligne(plateau):
    score = 0
    for iligne in range(len(plateau)):
        for icase in range(len(plateau[0])):
            if plateau[iligne][icase] is not None:
                try:
                    if plateau[iligne][icase] and plateau[iligne+1][icase] is not None and plateau[iligne+1][icase]:
                        score += 1 
                except IndexError:
                    pass
                try:
                    if plateau[iligne][icase] and plateau[iligne][icase-1] is not None and plateau[iligne][icase-1]:
                        score += 1 
                except IndexError:
                    pass
                try:
                    if not plateau[iligne][icase] and plateau[iligne+1][icase] is not None and not plateau[iligne+1][icase]:
                        score += 1 
                except IndexError:
                    pass
                try:
                    if not plateau[iligne][icase] and plateau[iligne][icase-1] is not None and not plateau[iligne][icase-1]:
                        score += 1 
                except IndexError:
                    pass
    return score

def score_noir(plateau) : 
    return 0

def plateaux_differents(plateau):
    combinaisons = calcul_combinaisons(plateau)
    n_case=0
    plateaux = []
    plateau_current = []
    for c in combinaisons:
        plateau_current = []
        for tab_case in plateau:
            ligne_current =[]
            for case in tab_case:
                if case:
                    if c[n_case]:
                        ligne_current.append(True)
                    else:
                        ligne_current.append(False)
                    n_case += 1
                else:
                    ligne_current.append(None)
            plateau_current.append(ligne_current)
        n_case = 0
        plateaux.append(plateau_current)


    return plateaux

#Piqué sur https://stackoverflow.com/questions/43816965/permutation-without-duplicates-in-python
def binary_permutations(tab_size, nb_true):
    for positions in itertools.combinations(range(tab_size), nb_true):
        p = [False] * tab_size
        for i in positions:
            p[i] = True
        yield p

def calcul_combinaisons(plateau):
    nb_ca = nb_cases(plateau)
    return binary_permutations(nb_ca,int(nb_ca/2))

def print_jeu(jeu):
    for tab_case in jeu:
        for case in tab_case:
            if case is None:
                print("   ", end='')
            elif case:
                print(" O ", end='')
            else:
                print(" X ", end='')
        print()
    print("Score :   diagonale : {} ligne: {}".format(score_diagonale(i),score_ligne(i)))

def print_plateau(plateau):
    print(plateau)
    for tab_case in plateau:
        for case in tab_case:
            if case:
                print(" O ", end='')
            else:
                print(" X ", end='')
        print()

def print_tout_les_jeux(plateau):
    for i in plateaux_differents(plateau):
        print_jeu(i)

def calcul_stats(plateau):
    somme_score_diag = 0
    somme_score_ligne = 0
    nb_victoires_diag = 0
    nb_victoires_lignes = 0
    nb_egalite = 0
    jeux = plateaux_differents(plateau)
    for jeu in jeux:
        somme_score_diag += score_diagonale(jeu)
        somme_score_ligne += score_ligne(jeu)
        if score_diagonale(jeu) > score_ligne(jeu) : 
            nb_victoires_diag +=1
        elif score_diagonale(jeu) < score_ligne(jeu):
            nb_victoires_lignes +=1
        else:
            nb_egalite+=1
    return len(jeux), somme_score_diag, somme_score_ligne, nb_victoires_diag, nb_victoires_lignes, nb_egalite

def print_stats(plateau):
    nb_jeux, somme_score_diag, somme_score_ligne, nb_victoires_diag, nb_victoires_lignes, nb_egalite = calcul_stats(plateau)
    print("""
    Nombre de possibilités:   {}
    Nombre victoires          diagonale: {}
                              lignes: {}
                              egalite: {}
            Moyenne           diagonale: {}
                              lignes: {}
    """.format(nb_jeux,nb_victoires_diag,nb_victoires_lignes,nb_egalite,somme_score_diag/nb_jeux,somme_score_ligne/nb_jeux))

def trouver_plateaux(nb_lignes = 4,nb_colonnes = 4, nb_cases=12):
    if nb_cases % 2 != 0 :
        raise ValueError("Le nombre de cases doit etre pair")
    nb_potentiel = nb_lignes * nb_colonnes
    permutations = list(binary_permutations(nb_potentiel,nb_cases))
    plateaux = []
    for p in permutations:
        plateau = []
        index = 0 
        ligne = []
        for case in p:
            if index == nb_colonnes :
                plateau.append(ligne)
                ligne = []
                index = 0
            ligne.append(case)
            index += 1
        plateau.append(ligne)
        plateaux.append(plateau)
    return plateaux

def trouver_meilleurs_plateaux(nb_lignes = 4,nb_colonnes = 4, nb_cases=12):
    plateaux = trouver_plateaux(nb_lignes,nb_colonnes,nb_cases)
    nb_plateaux = len(plateaux)
    index = 1
    nb_plateaux10 = nb_plateaux // 10
    #print("Nombre de plateaux à tester : {}".format(nb_plateaux))
    for plateau in plateaux:
        #print("{}/{}".format(index,nb_plateaux))
        nb_jeux, somme_score_diag, somme_score_ligne, nb_victoires_diag, nb_victoires_lignes, nb_egalite = calcul_stats(plateau)
        if abs(somme_score_diag/somme_score_ligne) > 0.99 and abs(nb_victoires_diag/nb_victoires_lignes) > 0.99 :
            print_plateau(plateau)
            print_stats(plateau)
        if index % nb_plateaux10 == 0:
            print("{}/{}".format(index,nb_plateaux))
        index += 1



if __name__ == "__main__":
    trouver_meilleurs_plateaux()